package Managers;

import java.awt.*;
import javax.swing.*;

import GUI.bodyComposer;
import GUI.gameComposer;
import GUI.initComposer;

import java.util.*;

public class mainWindowManager extends JFrame {
	private static final long serialVersionUID = 1L;
	public static Map<String, Component> panelReferences; // variable to save all component references

	public static initComposer initComposer;
	public static bodyComposer bodyComposer;
	public static gameComposer gameComposer;
	

	// Root window manager
	public mainWindowManager() {
		// JFrame setup
		this.setTitle("Master Manager");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Window and root container setup
		Toolkit tk = Toolkit.getDefaultToolkit();
		this.setLayout(new BorderLayout());
		this.setSize(800, 400);
		Dimension dim = tk.getScreenSize();
		int xPosCenter = (dim.width / 2) - (this.getWidth() / 2);
		int yPosCenter = (dim.height / 2) - (this.getHeight() / 2);
		this.setLocation(xPosCenter, yPosCenter);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		// Managed resources
		panelReferences = new TreeMap<String, Component>();
		bodyComposer = new bodyComposer();
		initComposer = new initComposer();
		gameComposer = new gameComposer();

		// ------- PANELS ---------
		mainWindowManager.panelAdder(bodyComposer.panelBodyCard, "panelInitCard", initComposer.panelInitCard, 0);
		mainWindowManager.panelAdder(bodyComposer.panelBodyCard, "panelBodyWelcome", bodyComposer.panelBodyWelcome, 1);
		mainWindowManager.panelAdder(bodyComposer.panelBodyCard, "panelBodyLore", bodyComposer.panelBodyLore, 2);
		mainWindowManager.panelAdder(bodyComposer.panelBodyCard, "panelBodyEditor", bodyComposer.panelBodyEditor, 10);
		
		
		this.add(bodyComposer.panelBodyCard, BorderLayout.CENTER);
		this.add(bodyComposer.panelNavBar, BorderLayout.NORTH);// Add instructions to the root frame
		this.add(bodyComposer.panelBottom, BorderLayout.SOUTH);
		

		
		/* ************************** FINAL SETUP ************************* */
		this.setVisible(true);
	}

	// <<>><<>><<>><<>><<>> METHODS <<>><<>><<>><<>><<>><<>>
	public static void panelAdder(JPanel panel, String Key, Component Value, String Layout) {
		panelReferences.put(Key, Value);
		panel.add(Value, Layout);
	}

	public static void panelAdder(JPanel panel, String Key, Component Value) {
		panelAdder(panel, Key, Value, null);
	}

	public static void panelAdder(JPanel panel, String Key, Component Value, int Layer) {
		panelReferences.put(Key, Value);
		/*System.out.println("=>-=>-=>-=>-=>-=>");
		System.out.println(panel);

		System.out.println(Key);

		System.out.println(Value);

		System.out.println(Layer);

		System.out.println("^^^^^^^^^^^^^^^^^^");*/
		panel.add(Value, String.valueOf(Layer));
	}

	public static void panelPopper(JPanel panel, String Key) {
		panel.remove(panelReferences.get(Key));
		panelReferences.remove(Key);
	}
}
