package Managers;

import javax.swing.SwingUtilities;
import com.alee.laf.WebLookAndFeel;
import com.alee.managers.CoreManagers;
import Enviroment.*;

public class mainManager {
	
	public final static short ENGLISH = 1;
	public final static short SPANISH = 2;
	
	public static short Lang = 1;
	

	public static void main(String[] args) {
		translatorDelegate.startEnv();
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				WebLookAndFeel.install();
				CoreManagers.initialize();

				@SuppressWarnings("unused")
				mainWindowManager mainWindow = new mainWindowManager();

				System.out.println("> Window loaded!");
			}
		});
	}
}