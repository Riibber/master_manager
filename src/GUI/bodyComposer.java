package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import Enviroment.translatorDelegate;
import Managers.mainManager;
import Managers.mainWindowManager;

public class bodyComposer {
	public JPanel panelBodyCard; // cards for the body layout
	public JPanel panelBodyLoreCard;
	public JPanel panelNavBar; // reference to the different panels, except the root panel
	public JPanel panelBottom;

	public JPanel panelBodyWelcome;
	public JPanel panelBodyWelcomeTop;
	public JPanel panelBodyLore;
	public JPanel panelBodyEditor;
	public JPanel panelBodyLoreGeography;
	public JPanel panelBodyLorePolitics;
	public JPanel panelBodyLorePlot;
	public JPanel panelBodyLoreEvidences;
	public JPanel panelBodyLoreNotes;
	public JPanel panelBodyCardMetaNotes;
	
	
	public JPanel panelCharacters;
	public JPanel panelScenes;

	public bodyComposer() {
		panelBodyCard = new JPanel(new CardLayout());
		panelNavBar = new JPanel(new GridLayout());
		panelBottom = new JPanel(new BorderLayout());
		panelBodyLoreCard = new JPanel(new CardLayout());

		panelBodyWelcome = new JPanel(new BorderLayout());
		panelBodyWelcomeTop = new JPanel(new FlowLayout());
		panelBodyLore = new JPanel(new GridLayout(2,3)); //Historia, Territorio/s, Politica actual, Trama, Pruebas, Notas
		panelBodyEditor = new JPanel(new BorderLayout());
		panelBodyLoreGeography = new JPanel(new FlowLayout());
		panelBodyLorePolitics = new JPanel(new FlowLayout());
		panelBodyLorePlot = new JPanel(new FlowLayout());
		panelBodyLoreEvidences = new JPanel(new FlowLayout());
		panelBodyLoreNotes = new JPanel(new FlowLayout());
		panelBodyCardMetaNotes = new JPanel(new CardLayout());

		mainWindowManager.panelAdder(panelNavBar, "panelNavBarStartMenuButton", new JButton(translatorDelegate.getText("panelNavBarStartMenuButton")));
		mainWindowManager.panelAdder(panelNavBar, "panelNavBarLorePlotNotesButton", new JButton(translatorDelegate.getText("panelNavBarLorePlotNotesButton")));
		mainWindowManager.panelAdder(panelNavBar, "panelNavBarScenesObjectsButtonPCNPC", new JButton("PC/NPC"));
		mainWindowManager.panelAdder(panelNavBar, "panelNavBarScenesObjectsButton", new JButton(translatorDelegate.getText("panelNavBarScenesObjectsButton")));
		mainWindowManager.panelAdder(panelNavBar, "panelNavBarScriptsButton", new JButton(translatorDelegate.getText("panelNavBarScriptsButton")));
		mainWindowManager.panelAdder(panelNavBar, "panelNavBarConfigButton", new JButton(translatorDelegate.getText("panelNavBarConfigButton")));

		// Bottom panel setup
		mainWindowManager.panelAdder(panelBottom, "Start Game", new JButton(translatorDelegate.getText("panelBottomStartGame")), BorderLayout.EAST);

		Label centralTextLabel = new Label(translatorDelegate.getText("centralTextLabel"), SwingConstants.CENTER);
		mainWindowManager.panelAdder(panelBodyWelcomeTop, "centralTextLabel", centralTextLabel);

		panelBodyWelcome.add(panelBodyWelcomeTop, BorderLayout.NORTH);
		

		mainWindowManager.panelAdder(panelBodyLore, "panelBodyLoreHistoryButton", new JButton(translatorDelegate.getText("panelBodyLoreHistoryButton")));
		mainWindowManager.panelAdder(panelBodyLore, "panelBodyLoreGeographyButton", new JButton(translatorDelegate.getText("panelBodyLoreGeographyButton")));
		mainWindowManager.panelAdder(panelBodyLore, "panelBodyLorePolitics", new JButton(translatorDelegate.getText("panelBodyLorePoliticsButton")));
		mainWindowManager.panelAdder(panelBodyLore, "panelBodyLorePlotButton", new JButton(translatorDelegate.getText("panelBodyLorePlotButton")));
		mainWindowManager.panelAdder(panelBodyLore, "panelBodyLoreEvidencesButton", new JButton(translatorDelegate.getText("panelBodyLoreEvidencesButton")));
		mainWindowManager.panelAdder(panelBodyLore, "panelBodyLoreNotesButton", new JButton(translatorDelegate.getText("panelBodyLoreNotesButton")));
		
		Label worldLoreLabel = new Label(translatorDelegate.getText("worldLoreLabel"));
		mainWindowManager.panelAdder(panelBodyEditor, "worldLoreLabel", worldLoreLabel, BorderLayout.NORTH);
		TextArea WorldLore = new TextArea(20, 20);
		mainWindowManager.panelAdder(panelBodyEditor, "WorldLore", WorldLore, BorderLayout.CENTER);

		panelNavBar.setVisible(false);
		panelBottom.setVisible(false);

		// -- -- -- -- -- -- -- -- LISTENERS -- -- -- -- -- -- -- --
		((JButton) mainWindowManager.panelReferences.get("panelNavBarStartMenuButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "1");
			}
		});

		((JButton) mainWindowManager.panelReferences.get("panelNavBarLorePlotNotesButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "2");
			}
		});

		((JButton) mainWindowManager.panelReferences.get("panelNavBarScenesObjectsButtonPCNPC")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "3");
			}
		});

		((JButton) mainWindowManager.panelReferences.get("panelNavBarScenesObjectsButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "4");
			}
		});
		
		((JButton) mainWindowManager.panelReferences.get("panelNavBarScriptsButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "1");
			}
		});
		
		((JButton) mainWindowManager.panelReferences.get("panelNavBarConfigButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "1");
			}
		});
		
		((JButton) mainWindowManager.panelReferences.get("panelBodyLoreHistoryButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "10");
			}
		});
		
		((JButton) mainWindowManager.panelReferences.get("panelBodyLoreGeographyButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "10");
			}
		});
		
		((JButton) mainWindowManager.panelReferences.get("panelBodyLoreEvidencesButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "10");
			}
		});
		
		((JButton) mainWindowManager.panelReferences.get("panelBodyLorePlotButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "10");
			}
		});
		
		((JButton) mainWindowManager.panelReferences.get("panelBodyLoreNotesButton")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "10");
			}
		});
		
		//TODO: Make each editor load and save separated text files.
		
		
		//--- LORE EDITOR BUTTONS ---//
		
		/*((JButton) mainWindowManager.panelReferences.get("panelBodyLoreHistory")).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) panelBodyCard.getLayout()).show(panelBodyCard, "4");
			}
		});*/
	}
}
