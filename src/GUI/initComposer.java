package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import Enviroment.translatorDelegate;
import Managers.mainManager;
import Managers.mainWindowManager;

public class initComposer {

	public JPanel panelInitCard;
	public JPanel panelInit;
	public JPanel panelInitLoad;
	public JPanel panelInitCreate;

	@SuppressWarnings("unchecked")
	public initComposer() {
		// Panels for Menus and group Components
		panelInitCard = new JPanel(new CardLayout());
		panelInit = new JPanel(new GridLayout(5, 1));
		panelInitLoad = new JPanel(new GridLayout(3, 1));
		panelInitCreate = new JPanel(new GridLayout(3, 1));

		// Startup menu
		JPanel panelInitBody = new JPanel(new BorderLayout());

		// Startup menu
		JPanel panelInitBodyLanguage = new JPanel(new FlowLayout());
		mainWindowManager.panelAdder(panelInitBodyLanguage, "chooseLanguageLabel",
				new Label(translatorDelegate.getText("chooseLanguageLabel")));
		String[] chooseLanguageArray = { "English", "Spanish" };
		mainWindowManager.panelAdder(panelInitBodyLanguage, "chooseLanguageJComboBox",
				new JComboBox<String>(chooseLanguageArray));
		mainWindowManager.panelAdder(panelInitBody, "panelInitBodyLanguage", panelInitBodyLanguage, BorderLayout.NORTH);

		JPanel panelInitBodyCreateOrLoad = new JPanel();
		mainWindowManager.panelAdder(panelInitBodyCreateOrLoad, "panelInitBodyLabel",
				new Label(translatorDelegate.getText("panelInitBodyLabel")), BorderLayout.CENTER);
		mainWindowManager.panelAdder(panelInitBodyCreateOrLoad, "panelInitBodyCreateButton", new JButton("Create"));
		mainWindowManager.panelAdder(panelInitBodyCreateOrLoad, "panelInitBodyLoadButton", new JButton("Load"));

		mainWindowManager.panelAdder(panelInitBody, "panelInitBodyCreateOrLoad", panelInitBodyCreateOrLoad); // TODO: Change
																										// name and
																										// other names
		
		JPanel panelInitTitle = new JPanel(new FlowLayout());
		mainWindowManager.panelAdder(panelInitTitle, "panelInitTitleLableSpc1", new Label(""));
		mainWindowManager.panelAdder(panelInitTitle, "panelInitTitleLable", new Label("Master Manager"));
		

		// We add the Startup menu setup to the Init panel that get's the JFrame's
		// content

		mainWindowManager.panelAdder(panelInit, "panelInitSpc1", new Label(""));
		mainWindowManager.panelAdder(panelInit, "panelInitTitle", panelInitTitle);
		mainWindowManager.panelAdder(panelInit, "panelInitBody", panelInitBody);
		mainWindowManager.panelAdder(panelInit, "panelInitSpc2", new Label(""));
		mainWindowManager.panelAdder(panelInit, "panelInitCredits", new Label(translatorDelegate.getText("panelInitCredits")));

		// Create campaign menu
		JPanel panelinitCreateForm = new JPanel();
		mainWindowManager.panelAdder(panelInitCreate, "panelInitCreateSpc1", new Label(""));

		mainWindowManager.panelAdder(panelinitCreateForm, "panelInitCreateGameLabel",
				new Label(translatorDelegate.getText("panelInitCreateGameLabel")));

		String[] arrayGames = { "Call of Cthulhu", "Pathfinder", "FATE" };
		JComboBox<String> selectArrayGames = new JComboBox<String>(arrayGames);

		mainWindowManager.panelAdder(panelinitCreateForm, "selectArrayGames", selectArrayGames);
		mainWindowManager.panelAdder(panelInitCreate, "panelinitCreateForm", panelinitCreateForm);

		JPanel panelInitCreateSend = new JPanel(new BorderLayout());
		mainWindowManager.panelAdder(panelInitCreateSend, "createCampaignButton",
				(new JButton(translatorDelegate.getText("createCampaignButton"))), BorderLayout.EAST);
		mainWindowManager.panelAdder(panelInitCreate, "panelInitCreateSend", panelInitCreateSend);

		// Load campaign menu
		Label selectLoadLabel = new Label(translatorDelegate.getText("selectSaveLabel"));

		String[] lastOpened = { "archivo1", "partidaCthulhu", "Ligma.png" };
		JComboBox<String> selectLoad = new JComboBox<String>(lastOpened);

		JPanel panelInitLoadSend = new JPanel(new BorderLayout());
		mainWindowManager.panelAdder(panelInitLoadSend, "loadCampaignButton",
				(new JButton(translatorDelegate.getText("loadCampaignButton"))), BorderLayout.EAST);

		JPanel panelInitLoadForm = new JPanel(new FlowLayout());

		mainWindowManager.panelAdder(panelInitLoadForm, "selectLoadLabel", selectLoadLabel);
		mainWindowManager.panelAdder(panelInitLoadForm, "selectLoad", selectLoad);

		mainWindowManager.panelAdder(panelInitLoad, "panelInitLoadFormSpc1", new Label(""));
		mainWindowManager.panelAdder(panelInitLoad, "panelInitLoadForm", panelInitLoadForm, BorderLayout.CENTER);
		mainWindowManager.panelAdder(panelInitLoad, "panelInitLoadSend", panelInitLoadSend);

		// Loader of all the Init panels
		mainWindowManager.panelAdder(panelInitCard, "panelInit", panelInit, 0);
		mainWindowManager.panelAdder(panelInitCard, "panelInitCreate", panelInitCreate, 2);
		mainWindowManager.panelAdder(panelInitCard, "panelInitLoad", panelInitLoad, 1);


		// -- -- -- -- -- -- -- LISTENERS -- -- -- -- -- -- -- --

		((JButton) mainWindowManager.panelReferences.get("panelInitBodyCreateButton"))
				.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mainWindowManager.getFrames()[0].setSize(400, 200);
						((CardLayout) panelInitCard.getLayout()).show(panelInitCard, "2");
					}
				});

		((JButton) mainWindowManager.panelReferences.get("createCampaignButton"))
				.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						mainWindowManager.getFrames()[0].setSize(800, 600);
						((CardLayout) mainWindowManager.bodyComposer.panelBodyCard.getLayout())
								.show(mainWindowManager.bodyComposer.panelBodyCard, "1");

						mainWindowManager.bodyComposer.panelNavBar.setVisible(true);
						mainWindowManager.bodyComposer.panelBottom.setVisible(true);
					}
				});

		((JButton) mainWindowManager.panelReferences.get("loadCampaignButton")).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mainWindowManager.getFrames()[0].setSize(800, 600);
				((CardLayout) mainWindowManager.bodyComposer.panelBodyCard.getLayout())
						.show(mainWindowManager.bodyComposer.panelBodyCard, "1");

				mainWindowManager.bodyComposer.panelNavBar.setVisible(true);
				mainWindowManager.bodyComposer.panelBottom.setVisible(true);
			}
		});

		((JButton) mainWindowManager.panelReferences.get("panelInitBodyLoadButton"))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						mainWindowManager.getFrames()[0].setSize(400, 200);
						((CardLayout) panelInitCard.getLayout()).show(panelInitCard, "1");
					}
				});

		((JComboBox<String>) mainWindowManager.panelReferences.get("chooseLanguageJComboBox"))
				.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String selectedItem = (String) ((JComboBox<String>) e.getSource()).getSelectedItem();
						System.out.println("> ComboBox Triggered! Language: "
								+ selectedItem);
						switch (selectedItem) {
						case "English":
							mainManager.Lang = mainManager.ENGLISH;
							translatorDelegate.updateText();
							System.out.println("> English loaded!");
							break;
						case "Spanish":
							mainManager.Lang = mainManager.SPANISH;
							translatorDelegate.updateText();
							System.out.println("> Spanish loaded!");
							break;
						}
					}
				});
	}
}