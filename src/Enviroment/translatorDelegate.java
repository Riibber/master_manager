package Enviroment;

import java.awt.Component;
import java.awt.Label;
import java.util.*;

import javax.swing.JButton;

import Managers.mainManager;
import Managers.mainWindowManager;

public class translatorDelegate {
	private static Iterator<String> finalValueIterator; //Used to update the component's texts

	public static Map<String, String> tEnglish = new TreeMap<String, String>();
	public static Map<String, String> tSpanish = new TreeMap<String, String>();

	public static void startEnv() {

		textRegister("centralTextLabel", "Bienvenido", "Welcome");
		
		textRegister("chooseLanguageLabel", "Escoja un idioma", "Choose a language");
		
		textRegister("selectSaveLabel", "Escoja archivo de partida:",
				"Choose a saved file to load:");
		
		textRegister("worldLoreLabel", "Introduzca el Lore del mundo", "Insert the world's Lore");
		textRegister("panelInitBodyLabel", "Escoja entre guardar o crear una nueva campaña",
				"Choose between create or load a campaign");
		
		textRegister("panelInitCreateGameLabel", "Escoja el sistema/juego en el que desea crear una campaña",
				"Choose the desired system/game to create a campaign");
		
		textRegister("createCampaignButton", "Crear campaña", "create Campaign");
		
		textRegister("loadCampaignButton", "Cargar campaña", "Load campaign");
		
		textRegister("panelNavBarStartMenuButton", "Menu Principal", "Start Menu");
		
		textRegister("panelNavBarLorePlotNotesButton", "Lore/Trama/Notas", "Lore/Plot/Notes");
		
		textRegister("panelNavBarScenesObjectsButton", "Escenas/Objetos", "Scenes/Objects");
		
		textRegister("panelBottomStartGame", "Comenzar partida", "Start Game");
		
		textRegister("panelNavBarConfigButton", "Configuracion", "Settings");
		
		textRegister("panelNavBarScriptsButton", "Scripts/Mods", "Scripts/Mods");
		
		textRegister("panelInitCredits", "Programa creado con Java, Swing, WebLaF y LWJGL; Robert Torrell Belzach -2018- ", "Program made with Java, Swingm, WebLaF and LWJGL; Robert Torrell Belzach -2018-");
		
		textRegister("panelBodyLoreHistoryButton", "Historia", "History");
	
		textRegister("panelBodyLoreGeographyButton", "Geografia y territorios", "Geography and terriories");
		
		textRegister("panelBodyLorePoliticsButton", "Politica", "Politics");
		
		textRegister("panelBodyLorePlotButton", "Trama", "Plot");
		
		textRegister("panelBodyLoreEvidencesButton", "Pruebas/Objectos clave", "Evidences/Key objects");
		
		textRegister("panelBodyLoreNotesButton", "Notas", "Notes");
	}

	private static void textRegister(String Key, String Spanish, String English) {
		tEnglish.put(Key, English);
		tSpanish.put(Key, Spanish);
	}
	

	public static String getText(String Key) {
		String outputText;
		switch (mainManager.Lang) {
		case mainManager.ENGLISH:
			outputText = translatorDelegate.tEnglish.get(Key);
			if (outputText == null) {
				throw new IllegalArgumentException("Not a Key!");
			}
			System.out.println("> Text sent! (English) Text: " + outputText);
			return outputText;

		case mainManager.SPANISH:
			outputText = translatorDelegate.tSpanish.get(Key);
			if (outputText == null) {
				throw new IllegalArgumentException("Not a Key!");
			}
			System.out.println("> Text sent! (Spanish) Text: " + outputText);
			return outputText;

		default:
			throw new IllegalArgumentException("Incorrect language!");
		}
	}

	public static void updateText() {
		switch(mainManager.Lang) {
		case mainManager.ENGLISH:
			finalValueIterator =  translatorDelegate.tEnglish.values().iterator();
			
			for (String actualValue : translatorDelegate.tSpanish.values()) {
				updateText(actualValue, finalValueIterator.next());
			}
			break;
		case mainManager.SPANISH:
			finalValueIterator =  translatorDelegate.tSpanish.values().iterator();
			
			for (String actualValue : translatorDelegate.tEnglish.values()) {
				updateText(actualValue, finalValueIterator.next());
			}
			break;
		default:
			throw new IllegalArgumentException("Incorrect language!");
		}
	}

	private static void updateText(String actualValue, String finalValue) {
		for (Component Comp : mainWindowManager.panelReferences.values()) {
			if (Comp instanceof Label && ((Label)Comp).getText().equals(actualValue)) {
				((Label) Comp).setText(finalValue);
				System.out.println("> Label updated! Label: " + actualValue);
			}
			else if (Comp instanceof JButton && ((JButton)Comp).getText().equals(actualValue)){
				((JButton) Comp).setText(finalValue);
			}
		}
	}
}