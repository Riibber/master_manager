# Master Manager

A local desktop app to manage Tabletop Roleplay Games.

It will be designed to be used just like some Videogame editor or game engine (but
without all the required technical knowledge). It will be a centraliced tool so
Roleplaying online gets as easier and comfy as opening a Steam game, and playing
on local makes the experience smoother.

This app will be able to:

    -Manage PC and NPC sheets (and create/customize your own)
    -Autogenerate a bunch of things (NPC, random encounters, etc...)
    -Specify Automaticed parameters (e.g. Autobalance: create a combat map that, 
    depending on the state of the party, some things will or not spawn)
    -Organize all GM and Campaign information (anotations, logs, world maps, reference system...)
    -Give tons of atmospheric tools to enhance the experience (e.g. HUD and map system
    to genenare in a visually pleasing way all the world's information)
    -Manage and create Gamemodes and rules (turn-based HUD, info displayer, etc...)
    -VoIP with the users and customize it's behavior (e.g. only let players hear 
    what their PC have near)